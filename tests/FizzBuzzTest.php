<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

  class FizzBuzzTest extends TestCase
{

  protected $fizzBuzz;

  public function setUp()
  {
    $fizzBuzz = new App\FizzBuzz();
  }

  public function fizzBuzz($expected, $number)
  {
    $fizzBuzz = new App\FizzBuzz();
    $this->assertEquals($expected, $fizzBuzz->check($number));
  }

  public function testFizzBuzz()
  {
    $this->fizzBuzz(1,1);
  }

}