@extends('layouts.loca')

@section('conteudo')
  <h1 class="ls-title-intro ls-ico-cart">Produtos</h1>

  @if(Session::has('flash_message'))
      <div class="col-md-12col-md-offset-1">
          <div align="center" class="alert {{ Session::get('flash_message')['class'] }}">
              {{ Session::get('flash_message')['msg'] }}
               <span data-ls-module="dismiss" class="ls-dismiss">&times;</span>
          </div>
      </div>
  @endif

  <a href="{{route('products.insert')}}" class="ls-btn-primary">Cadastrar novo</a>

  <div class="ls-box-filter">
    <form action="{{route('products.search')}}" method="get" class="ls-form ls-form-inline ls-float-left">
      <label class="ls-label" role="search">
        <b class="ls-label-text ls-hidden-accessible">Nome do Produto</b>
        <input type="text" name="palavra_buscada" placeholder="Nome do Produto" class="ls-field-sm">
      </label>
        <input type="submit" value="Buscar" class="ls-btn ls-btn-sm" title="Buscar">
    </form>
  </div>

  <table class="ls-table">
    <thead>
      <tr>
        <th>ID</th>
        <th>Nome</th>
        <th>Quantidade</th>
        <th>Ações</th>
      </tr>
    </thead>
    @foreach($products as $product)
      <tbody>
          <tr>
            <td>{{$product->id}}</td>
            <td>{{$product->name}}</td>
            <td>{{$product->amount}}</td>
            <td class="ls-regroup ">
              <div data-ls-module="dropdown" class="ls-dropdown ">
                <a href="#" class="ls-btn ls-btn-sm ">Administrar</a>
                <ul class="ls-dropdown-nav">
                  <li><a href="{{route('products.edit',$product->id)}}">Alterar</a></li>
                  <li><a href="javascript:(confirm('Deletar esse registro?') ? window.location.href='{{route('products.remove', $product->id)}}' : false)" class="ls-color-danger">Excluir</a></li>
                </ul>
              </div>
            </td>
          </tr>
      </tbody>
    @endforeach
  </table>
@endsection
<h1>Hello</h1>
@section('foot')
  <ul class="ls-pager">
     @include('layouts.pedro',['paginator' => $products])
  </ul>
@endsection

