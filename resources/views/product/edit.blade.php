@extends('layouts.loca')

@section('conteudo')
 <h1 class="ls-title-intro ls-ico-users">Editar Produto</h1>

<form action="{{route('products.update', $produto->id)}}" method="post" class="ls-form">
<input type="hidden" name="_token" value="{{ csrf_token() }}">
<input type="hidden" name="_method" value="put">
  <div class="row">
    <label class="ls-label col-md-6">
      <span class="ls-label-text">Nome</span>
      <input type="text" name="name" value="{{$produto->name}}">
    </label>
  </div>
  <div class="row">
    <label class="ls-label col-md-6">
      <span class="ls-label-text">Quantidade</span>
     <input type="number" name="amount" readonly="readonly" value="{{$produto->amount}}">
    </label>
  </div>

  <button type="submit" class="ls-btn-primary">Salvar</button>
  <a href="{{route('products.index')}}" class="ls-btn">Cancelar</a>
</form>
@endsection