@extends('layouts.loca')

@section('conteudo')
 <h1 class="ls-title-intro ls-ico-users">Cadastrar Produto</h1>


<form action="{{route('products.save')}}" method="post" class="ls-form">
<input type="hidden" name="_token" value="{{ csrf_token() }}">
  <div class="row">
    <label class="ls-label col-md-6">
      <span class="ls-label-text">Nome</span>
      <input type="text" name="name">
    </label>
  </div>
  <div class="row">
    <label class="ls-label col-md-6">
      <span class="ls-label-text">Quantidade</span>
      <input type="number" name="amount">
    </label>
  </div>

  <button type="submit" class="ls-btn-primary">Salvar</button>
  <a href="{{route('products.index')}}" class="ls-btn">Cancelar</a>
</form>
@endsection