<!DOCTYPE html>
<html class="ls-theme-turquoise">
  <head>
    <title>LaraEstoque</title>

    <meta charset="utf-8">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta name="description" content="Insira aqui a descrição da página.">
    <link href="http://assets.locaweb.com.br/locastyle/3.8.5/stylesheets/locastyle.css" rel="stylesheet" type="text/css">
    <link rel="icon" sizes="192x192" href="/locawebstyle/assets/images/ico-boilerplate.png">
    <link rel="apple-touch-icon" href="/locawebstyle/assets/images/ico-boilerplate.png">

    <script type="text/javascript" src="https://www.google.com/jsapi"></script>

    <script type="text/javascript">

      google.load("visualization", "1", {packages:["corechart"]});

      function drawChart() {

        var data = google.visualization.arrayToDataTable([

          ['Situação do Estoque', 'Quando gosto dela'],
          ['Estoque Positivo', {{$positivo}}],
          ['Estoque Negativo', {{$negativo}}],
          ['Estoque Zerado', {{$zerado}}]
          ]);

      var options = {
          title:'Situação do Estoque',
          is3D:false
      };

      var chart = new google.visualization.PieChart(document.getElementById('chart_div'));

      chart.draw(data, options);
      }

      google.setOnLoadCallback(drawChart);

    </script>

  </head>
  <body>
    <div class="ls-topbar ">
  <!-- Barra de Notificações -->
    <div class="ls-notification-topbar">
    <div class="ls-alerts-list">
      <a href="#" class="ls-ico-bell-o" data-counter="{{$count or 0}}" data-ls-module="topbarCurtain" data-target="#ls-notification-curtain"><span>Notificações</span></a>
    </div>

      <!-- Dropdown com detalhes da conta de usuário -->
      <div data-ls-module="dropdown" class="ls-dropdown ls-user-account">
        <a href="#" class="ls-ico-user">
          <span class="ls-name">{{ Auth::user()->name }}</span>
        </a>

        <nav class="ls-dropdown-nav ls-user-menu">
          <ul>
            <li><a href="{{ url('/logout') }}">Sair</a></li>
           </ul>
        </nav>
      </div>
    </div>

    <!-- Nome do produto/marca com sidebar -->
      <h1 class="ls-brand-name">
        <a href="#" class="ls-ico-earth">
          LaraEstoque
        </a>
      </h1>

    <!-- Nome do produto/marca sem sidebar quando for o pre-painel  -->
    </div>

    <aside class="ls-sidebar">
    <div class="ls-sidebar-inner">
        <nav class="ls-menu">
          <ul>
             <li><a href="{{route('dashboard.index')}}" class="ls-ico-dashboard" title="Dashboard">Dashboard</a></li>
             <li><a href="{{route('users.index')}}" class="ls-ico-users" title="Usuarios">Usuários</a></li>
             <li><a href="{{route('products.index')}}" class="ls-ico-cart" title="Produtos">Produtos</a></li>
             <li><a href="{{route('stock.index')}}" class="ls-ico-pencil2" title="Relatórios da revenda">Estoques</a></li>
          </ul>
        </nav>
    </div>
    </aside>

    <aside class="ls-notification">
    <nav class="ls-notification-list" id="ls-notification-curtain">
      <h3 class="ls-title-2">Notificações</h3>
      <ul>
          @foreach($notifications as $notify)
            <li class="ls-dismissable">
              <a href="{{route('stock.filterNotify',$notify->id)}}">Produto {{$notify->name}} está com estoque negativo. Verifique.</a>
              <a href="{{ route('notification.delete', $notify->id) }}" data-ls-module="dismiss" class="ls-ico-close ls-close-notification"></a>
            </li>
          @endforeach
      </ul>
      <p class="ls-no-notification-message">Não há notificações.</p>
    </nav>
  </aside>

      <main class="ls-main ">
        <div class="container-fluid">
          <h1 class="ls-title-intro ls-ico-dashboard">Dashboard</h1>

          @linechart('Temps', 'temps_div')
          <div class="ls-box ls-board-box">
            <header class="ls-info-header">
              <h2 class="ls-title-3">Histórico do Estoque</h2>
            </header>
            <div id="temps_div"></div>
          </div>
          <div class="ls-box ls-board-box">

          <header class="ls-info-header">
              <h2 class="ls-title-3">Situação do Estoque</h2>
          </header>
          <div id="chart_div"></div>
          </div>
          <footer class="ls-footer" role="contentinfo">
           @yield('foot')
          </footer>
      </main>

      <!-- We recommended use jQuery 1.10 or up -->
      <script type="text/javascript" src="http://code.jquery.com/jquery-2.1.4.min.js"></script>
      <script src="http://assets.locaweb.com.br/locastyle/3.8.5/javascripts/locastyle.js" type="text/javascript"></script>
  </body>
</html>''