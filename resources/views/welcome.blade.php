<!DOCTYPE html>
<html class="ls-theme-gray">
<head>
  <meta charset="utf-8">
  <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
  <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
  <title>Tela de Login</title>
  <meta name="description" content="" />
  <meta name="keywords" content="" />
  <link href="http://assets.locaweb.com.br/locastyle/3.8.5/stylesheets/locastyle.css" rel="stylesheet" type="text/css">

</head>
  <body class="documentacao documentacao_exemplos documentacao_exemplos_login-screen documentacao_exemplos_login-screen_index">

  <div class="ls-login-parent">
    <div class="ls-login-inner">
      <div class="ls-login-container">
        <div class="ls-login-box">
    <h1 class="ls-login-logo"><img title="Logo login" src="https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcSTPIoZ3Y2uCV80HeUzymZo_geMvKnvM4LZU5ZiQfnQR5qwAvBF" /></h1>
    <form role="form" class="ls-form ls-login-form" method="POST" action="{{ url('/login') }}">
    {{ csrf_field() }}
      <fieldset>

        <label class="ls-label">
          <input id="email" name="email" class="ls-login-bg-user ls-field-lg" type="text" placeholder="E-mail" required autofocus>
        </label>
        <label class="ls-label">
            <input id="password" name="password" class="ls-login-bg-password ls-field-lg" type="password" placeholder="Senha" required>
        </label>
        <input type="submit" value="Entrar" class="ls-btn-primary ls-btn-block ls-btn-lg">
        <p class="ls-txt-center ls-login-signup"><a href="{{ url('/register') }}">Cadastre-se agora</a></p>

      </fieldset>
    </form>
  </div>
      </div>
    </div>
  </div>

  <script type="text/javascript" src="http://code.jquery.com/jquery-2.1.4.min.js"></script>
  <script src="http://assets.locaweb.com.br/locastyle/3.8.5/javascripts/locastyle.js" type="text/javascript"></script>
</body>
</html>
