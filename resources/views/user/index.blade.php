@extends('layouts.loca')

@section('conteudo')
<h1 class="ls-title-intro ls-ico-users">Usuários</h1>

<a href="#" class="ls-btn-primary">Cadastrar novo</a>

<div class="ls-box-filter">
  <form  action="{{route('users.search')}}" method="get" class="ls-form ls-form-inline ls-float-left">
    <label class="ls-label" role="search">
      <b class="ls-label-text ls-hidden-accessible">Nome do Usuário</b>
      <input type="text" name="palavra_buscada" placeholder="Nome do Usuário" class="ls-field-sm">
    </label>
    <div class="ls-actions-btn">
      <input type="submit" value="Buscar" class="ls-btn ls-btn-sm" title="Buscar">
    </div>
  </form>
</div>

<table class="ls-table">
  <thead>
    <tr>
      <th>ID</th>
      <th>Nome</th>
      <th>E-mail</th>
    </tr>
  </thead>
  @foreach($users as $user)
  <tbody>
      <tr>
        <td>{{$user->id}}</td>
        <td>{{$user->name}}</td>
        <td>{{$user->email}}</td>

  </tbody>
  @endforeach
</table>

{!! $users->links() !!}

</div>


    </div>

@endsection