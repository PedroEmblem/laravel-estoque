@extends('layouts.loca')

@section('conteudo')

<h1 class="ls-title-intro ls-ico-pencil2">Estoques</h1>

    @if(Session::has('flash_message'))
      <div class="col-md-10 col-md-offset-1">
          <div align="center" class="alert {{ Session::get('flash_message')['class'] }}">
              {{ Session::get('flash_message')['msg'] }}
          </div>
      </div>
    @endif

 <div class="ls-box-filter">
    <form action="{{route('stock.search')}}" method="get" class="ls-form ls-form-inline ls-float-left">
      <label class="ls-label" role="search">
        <b class="ls-label-text ls-hidden-accessible">Nome do Produto</b>
        <input type="text" name="palavra_buscada" placeholder="Nome do Produto" class="ls-field-sm">
      </label>
      <div class="ls-actions-btn">
        <input type="submit" value="Buscar" class="ls-btn ls-btn-sm" title="Buscar">
      </div>
    </form>
 </div>


<form action="{{route('stock.insert')}}" method="post">
 {{ csrf_field() }}
  <table class="ls-table ">
    <thead>
      <tr>
        <th>ID Produto</th>
        <th>Nome</th>
        <th>Quantidade Atual</th>
        <th>Histórico</th>
        <th>+/-</th>
      </tr>
    </thead>

    @foreach($products as $prod)
    <tbody class="ls-sm-space">
      <tr>
        <td>{{$prod->id}}</td>
        <td>{{$prod->name}}</td>
        <td>{{$prod->sum}}</td>
        <td><a type="button" class="ls-btn-info" href="{{route('stock.store', $prod->id)}}">Histórico</a></td>
        <td><input type="number" name="{{$prod->id}}"></td>
      </tr>
    </tbody>
    @endforeach

    <input class="ls-btn-primary ls-float-right " type="submit" value="Alterar">

  </table>
  </form>
  @endsection

  @section('foot')

    <div>
       <ul class="ls-pager">
         @include('layouts.pedro',['paginator' => $products])
       </ul>
    </div>

  @endsection