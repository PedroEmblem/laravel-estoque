@extends('layouts.loca')

@section('conteudo')
  <h1 class="ls-title-intro ls-ico-list2">Histórico</h1>

   @if(Session::has('flash_message'))
                <div class="col-md-10 col-md-offset-1">
                    <div align="center" class="alert {{ Session::get('flash_message')['class'] }}">
                        {{ Session::get('flash_message')['msg'] }}
                    </div>
                </div>
    @endif

  <table class="ls-table">
    <thead>
      <tr>
        <th>Produto</th>
        <th>Usuário</th>
        <th>Quantidade</th>
        <th>Data</th>
      </tr>
    </thead>
    @foreach($stores as $store)

    <tbody>
        <tr style="@if(($store->amount) <= 0){{'color:red'}}@else{{'color:green'}}@endif">
          <td>{{$store->nome_prod}}</td>
          <td>{{$store->user_name}}</td>
          <td>{{$store->amount}}</td>
          <td>{{date('d/m/Y', strtotime($store->created_at))}}</td>
        </tr>
    </tbody>

    @endforeach

  </table>
  {{-- <a class="ls-btn-info" href="{{route('stock.index')}}">Voltar</a>

    <ul class="ls-pager">
    @include('layouts.pedro',['paginator' => $stores])
    </ul> --}}

@endsection

@section('foot')

  <div>
     <ul class="ls-pager">
     <a class="ls-btn-info ls-float-left" href="{{route('stock.index')}}">Voltar</a>
       @include('layouts.pedro',['paginator' => $stores])
     </ul>
  </div>

@endsection