<!DOCTYPE html>
<html class="ls-theme-green">
<head>
  <meta charset="utf-8">
  <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
  <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
  <title>Registro</title>
  <meta name="description" content="" />
  <meta name="keywords" content="" />
  <link href="http://assets.locaweb.com.br/locastyle/3.8.5/stylesheets/locastyle.css" rel="stylesheet" type="text/css">
</head>
<body>
  <div class="container">
    <div class="row">
      <div class="ls-box ls-txt-center-all ls-width-500 ">
      <h5 class="ls-title-3 col-md-10 ">Registre-se</h5>

      <form method="POST" action="{{ url('/register') }}" class="ls-form row">
      {{ csrf_field() }}
        <fieldset>
          <label class="ls-label col-md-10">
            <b class="ls-label-text">Nome</b>
            <input type="text" name="name" placeholder="Digite seu nome" value="{{ old('name') }}" required>
          </label>
          <label class="ls-label col-md-10">
           <b class="ls-label-text">E-mail</b>
            <input type="text" name="email" placeholder="Digite seu email" value="{{ old('email') }}" required>
          </label>
          <label class="ls-label col-md-10">
            <b class="ls-label-text">Password</b>
            <input type="password" name="password" placeholder="Senha" required >
          </label>
          <label class="ls-label col-md-10">
            <b class="ls-label-text">Confirm-Password</b>
            <input type="password" name="password_confirmation" placeholder="Confirmar senha" required >
          </label>
          <label class="ls-label col-md-12 col-md-offset-4">
            <input type="submit" class="ls-btn-primary ls-v-align-middle" name="Registrar" value="Registrar">
          </label>
        </fieldset>
      </form>
      </div>
    </div>
  </div>
  <script type="text/javascript" src="http://code.jquery.com/jquery-2.1.4.min.js"></script>
  <script src="http://assets.locaweb.com.br/locastyle/3.8.5/javascripts/locastyle.js" type="text/javascript"></script>
</body>
</html>