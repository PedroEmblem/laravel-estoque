@if ($paginator->lastPage() > 1)
<ul class="ls-pagination">
    <div class="ls-group-btn">
        <li class="{{ ($paginator->currentPage() == 1) ? 'ls-disabled' : '' }}">
            <a class= "ls-ico-chevron-left" href="{{ $paginator->url($paginator->currentPage()-1) }}"></a>
        </li>
        @for ($i = 1; $i <= $paginator->lastPage(); $i++)
            <li class="{{ ($paginator->currentPage() == $i) ? 'ls-disabled ls-active' : '' }}">
                <a href="{{ $paginator->url($i) }}">{{ $i }}</a>
            </li>
        @endfor
        <li class="{{ ($paginator->currentPage() == $paginator->lastPage()) ? 'ls-disabled' : '' }}">
            <a class= "ls-ico-chevron-right" href="{{ $paginator->url($paginator->currentPage()+1) }}" ></a>
        </li>
    </div>
</ul>
@endif