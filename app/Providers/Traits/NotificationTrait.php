<?php

namespace App\Providers\Traits;

use Carbon\Carbon;

use App\Product;


trait NotificationTrait {

  public function countNotification()
  {
    return Product::where('notification', '=', true)
                   ->where('updated_at', '>', Carbon::yesterday())
                   ->where('status', '=', true)
                   ->count();
  }
  public function getNotification()
  {
    return Product::where('notification', '=', true)
                   ->where('updated_at', '>', Carbon::yesterday())
                   ->where('status', '=', true)
                   ->get();
  }
}