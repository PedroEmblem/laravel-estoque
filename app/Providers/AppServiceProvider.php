<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use View;
use Carbon\Carbon;
use App\Providers\Traits\NotificationTrait;

class AppServiceProvider extends ServiceProvider
{
    use NotificationTrait;
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::share('count', $this->countNotification());
        View::share('notifications', $this->getNotification());
        // duashduasidhuh
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
