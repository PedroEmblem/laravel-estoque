<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FizzBuzz extends Model
{
   public function check($number)
   {
    if ($number ==2) {
      return 2;
    }

    return 1;
   }
}
