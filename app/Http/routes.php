	<?php

use App\Product;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    if(Auth::guest())
    return view('welcome');
    else
      return view('dashboard.index');
});

Route::get('/notification/delete/{id}',['as' => 'notification.delete', function ($id) {
  $prod = Product::find($id);
  $prod->notification = false;
  $prod->save();
  return back();
}]);

Route::get('/home', 'HomeController@index');

//Autenticação
Route::auth();

//Dashboard
Route::get('/dashboard',               ['uses' => 'DashboardController@index', 'as' => 'dashboard.index']);

//Usuarios
Route::get('/usuarios',                ['uses' => 'UserController@index',  'as' => 'users.index']);
Route::get('/usuarios/buscar',         ['uses' => 'UserController@search', 'as' => 'users.search']);


//Produtos
Route::get ('/produtos',                ['uses' => 'ProductController@index',  'as' => 'products.index']);
Route::get ('/produtos/adicionar',      ['uses' => 'ProductController@insert', 'as' => 'products.insert']);
Route::post('/produtos/salvar',         ['uses' => 'ProductController@save',   'as' => 'products.save']);
Route::get ('/produtos/editar/{id}',    ['uses' => 'ProductController@edit',   'as' => 'products.edit']);
Route::put ('/produtos/atualizar/{id}', ['uses' => 'ProductController@update', 'as' => 'products.update']);
Route::get ('/produtos/excluir/{id}',   ['uses' => 'ProductController@remove', 'as' => 'products.remove']);
Route::get ('/produtos/buscar',         ['uses' => 'ProductController@search', 'as' => 'products.search']);

//Estoque
Route::get ('/estoque',                 ['uses' => 'StockController@index',  'as' => 'stock.index']);
Route::post('/estoque/insert',          ['uses' => 'StockController@insert', 'as' => 'stock.insert']);
Route::post('/estoque/update',          ['uses' => 'StockController@update', 'as' => 'stock.update']);
Route::get ('/estoque/store/{id}',      ['uses' => 'StockController@store',  'as' => 'stock.store']);
Route::get ('/estoque/buscar',          ['uses' => 'StockController@search', 'as' => 'stock.search']);
Route::get ('/estoque/buscar/{id}',     ['uses' => 'StockController@filterNotify', 'as' => 'stock.filterNotify']);


