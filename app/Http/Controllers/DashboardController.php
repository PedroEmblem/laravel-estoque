<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Product;
use App\Stock;
use DB;
use Khill\Lavacharts\Lavacharts;


class DashboardController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Product $product, Stock $stock) {
      //SELECT DISTINCT DATe(created_at) from stocks order by  DATe(created_at)
      $temperatures = \Lava::DataTable();
      $temperatures->addDateColumn('date');

      $datas = DB::table('stocks')
                          ->distinct()
                          ->orderBy(DB::raw('created_at'))
                          ->lists(DB::raw('date(created_at)'));

      $prod = DB::table('products')
                          ->orderBy('id')
                          ->where('status', '=' ,1)
                          ->lists('id');
      $prodnome = DB::table('products')
                          ->orderBy('id')
                          ->where('status', '=' ,1)
                          ->lists('name');

      // dd($prodnome);
      foreach($prodnome as $n)
      {
        $temperatures->addNumberColumn($n);
      }

      foreach($datas as $data)
      {
        $param = [];
        array_push($param, $data);
        // dd($datas);
        foreach($prod as $p)
        {
          $soma = Product::selectRaw('coalesce(sum(stocks.amount),0) + products.amount as sum')
                          ->leftJoin('stocks', 'products.id', '=', 'stocks.product_id')
                          ->where('products.status', '=', 1)
                          ->where('stocks.created_at', '<', $data)
                          ->where('stocks.product_id','=',$p)
                          ->get();
          array_push($param, $soma[0]['sum']);
        }
        $temperatures->addRow($param);
      // dd($param);
      }





      $products = $product->groupBy('products.updated_at','products.id','products.name','products.amount','products.status')
                          ->selectRaw('products.*, (coalesce(sum(stocks.amount),0) + products.amount) as sum')
                          ->leftJoin('stocks', 'products.id', '=', 'stocks.product_id')
                          ->where('products.status', '=', 1)
                          ->orderBy('products.name', 'asc')
                          ->get();

      $positivo = 0;
      $negativo = 0;
      $zerado = 0;

      foreach ($products as $key => $product) {

        if ($product->sum < 0) {
          $negativo ++;
        }else if ($product->sum == 0) {
          $zerado ++;
        }else {
          $positivo ++;
        }
      }




      \Lava::LineChart('Temps', $temperatures, [
          'title' => 'Estoques por data'
      ]);

        return view('dashboard.index', compact('positivo','negativo','zerado'));

      // return view('dashboard.index');
    }


    public function sum_amount($id)
    {
      $sum_amount = Product::groupBy('products.updated_at')
                             ->selectRaw('(coalesce(sum(stocks.amount),0) + products.amount) as sum')
                             ->leftJoin('stocks', 'products.id', '=', 'stocks.product_id')
                             ->find($id);
      return $sum_amount['sum'];
    }

}
