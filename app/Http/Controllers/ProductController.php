<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Product;
use App\Stock;
use Session;

class ProductController extends Controller
{
    protected $product, $session, $request;
    public function __construct(Product $prod, Session $ses, Request $req)
    {
        $this->middleware('auth');
        $this->product = $prod;
        $this->session = $ses;
        $this->request = $req;
    }

    public function
    {
        $products = $this->product->groupBy('products.updated_at','products.id','products.name','products.status')
                          ->selectRaw('products.*, coalesce(sum(stocks.amount),0) as amount')
                          ->leftJoin('stocks', 'products.id', '=', 'stocks.product_id')
                          ->where('products.status', '=', 1)//  1 = Ativo
                          ->orderBy('products.name', 'asc')//Ordem Alfabética
                          ->paginate(10);
        return $produtcs;
    }


    public function index()
    {
       $prod = buscarTodosProdutos();
       return view('product.index', compact('prod'));
    }

    public function insert()
    {
    	return view('product.insert');
    }

    public function save()
    {
        $qtde = $this->request['amount'];
        $this->product->create(['name' => $this->request['name'],'notification' => ($qtde < 0)]);
        if ($qtde != 0) {
            $id = $this->product->max('id');
            Stock::create(['product_id' => $id, 'amount' => $qtde, 'user_id' => \Auth::user()->id]);
        }

    	\Session::flash('flash_message',[
    			'msg'=>"Produto adicionado com Sucesso!",
    			'class'=>"ls-alert-success ls-dismissable"
    		]);

    	return redirect()->route('products.index');
    }

    public function edit($id)
    {
        $produto = buscarTodosProdutos();
        $produto->find($id);

        if(!$produto){
            \Session::flash('flash_message',[
                'msg'  =>"Produto não cadastrado!",
                'class'=>"alert-danger"
            ]);
        return redirect()->route('products.insert');
        }



        return view('product.edit', compact('produto'));

    }

    public function update($id)
    {
        $produto = $this->product->find($id)->update($this->request->all());


           \Session::flash('flash_message',[
                'msg'  =>"Produto alterado com Sucesso!",
                'class'=>"ls-alert-success ls-dismissable"
            ]);

            return redirect()->route('products.index');
    }

    public function remove($id)
    {
        try{
                // $produto = $this->product->find($id)->delete();
                $produto = $this->product->find($id);
                $produto->status = false;
                $produto->save();

                \Session::flash('flash_message',[
                'msg'  =>"Produto Removido com Sucesso!",
                'class'=>"ls-alert-success ls-dismissable"
            ]);

           }
           catch(\Exception $e)
           {
            \Session::flash('flash_message',[
                'msg'  =>"Produto Não pode ser removido!",
                'class'=>"ls-alert-danger ls-dismissable"
            ]);
           }

        return redirect()->route('products.index');
    }

    public function search()
    {

        $palavra_buscada = $this->request->get('palavra_buscada');

        $products = $this->product->where('name', 'LIKE', "%$palavra_buscada%")
                            ->where('status', '=', 1)
                            ->paginate(10);

        return view('product.index', compact('products'));
    }
}
