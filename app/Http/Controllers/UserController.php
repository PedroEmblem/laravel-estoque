<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index() 
    {
      $users = \App\user::paginate(10);

      return view('user.index ', compact('users'));
    }

    public function search(User $user, Request $request)
    {
    	$palavra_buscada = $request->get('palavra_buscada');

    	$users = $user->where('name', 'LIKE', "%$palavra_buscada%")->paginate(10);
    	// dd($users);

    	return view('user.index', compact('users'));
    }
}
