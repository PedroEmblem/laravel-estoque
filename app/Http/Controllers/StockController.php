<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Stock;
use App\Product;
use View;

class StockController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Stock $stock, Product $product)
    {
      $products = $product->groupBy('products.updated_at','products.id','products.name','products.amount','products.status')
                          ->selectRaw('products.*, (coalesce(sum(stocks.amount),0) + products.amount) as sum')
                          ->leftJoin('stocks', 'products.id', '=', 'stocks.product_id')
                          ->where('products.status', '=', 1)
                          ->orderBy('products.name', 'asc')
                          ->paginate(10);

      return view('stock.index', compact('products'));
    }

    public function insert(Request $request, Product $product, Stock $stock, \Auth $auth)
    {
      $request = $request->except('_token');

      foreach ($request as $key => $req) {

        if (!empty($req)) {

          $stock->create(['product_id' => $key,'user_id' => $auth::user()->id, 'amount' => $req]);

            $produto = $product->find($key);
            $produto->notification = $this->sum_amount($stock, $product, $key);
            $produto->save();


        }
      }
      return redirect()->route('stock.index');
    }

    public function store(Stock $stock, $id)
    {
      $stores = $stock    ->selectRaw('products.name as nome_prod, users.name as user_name, stocks.*')
                          ->join('products', 'products.id', '=', 'stocks.product_id')
                          ->join('users', 'users.id', '=', 'stocks.user_id')
                          ->where('stocks.product_id', '=', $id)
                          ->orderBy('stocks.created_at', 'desc')
                          ->paginate(10);

      return view('stock.store', compact('stores'));
    }

    public function sum_amount(Stock $stock, Product $product, $id)
    {
      $sum_amount = $product->groupBy('products.updated_at')
                             ->selectRaw('(coalesce(sum(stocks.amount),0) + products.amount) as sum')
                             ->leftJoin('stocks', 'products.id', '=', 'stocks.product_id')
                             ->find($id);
      $sum_amount['sum'];

      return $sum_amount['sum'] < 0;
    }

    public function search(Request $request)
    {
      if ($request['palavra_buscada'] != null) {
      $palavra_buscada = $request->get('palavra_buscada');

      $products = Product::where('name', 'LIKE', "%$palavra_buscada%")
                          ->where('products.status', '=', 1)
                          ->orderBy('products.name', 'asc')
                          ->paginate(10);

      return view('stock.index', compact('products'));
      }
      else
         return redirect()->route('stock.index');
    }

    public function filterNotify (Stock $stock, Product $product,$id)
    {
      $products = $product->groupBy('products.updated_at','products.id','products.name','products.amount','products.status')
                          ->selectRaw('products.*, (coalesce(sum(stocks.amount),0) + products.amount) as sum')
                          ->leftJoin('stocks', 'products.id', '=', 'stocks.product_id')
                          ->where('products.status', '=', 1)
                          ->where('products.id', '=', $id )
                          ->orderBy('products.name', 'asc')
                          ->paginate(10);

      return view('stock.index', compact('products'));

    }



}




























